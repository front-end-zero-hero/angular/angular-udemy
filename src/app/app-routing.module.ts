import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PipeTransformComponent} from "./pipe-transform/pipe-transform.component";
import {HttpRequestComponent} from "./http-request/http-request.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'http-request',
    pathMatch: 'full'
  },
  {
    path: 'pipe-transform',
    component: PipeTransformComponent
  },
  {
    path: 'http-request',
    component: HttpRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
