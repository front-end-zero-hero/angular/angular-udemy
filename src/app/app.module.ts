import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PipeTransformComponent } from './pipe-transform/pipe-transform.component';
import {ShortenPipe} from "./pipe-transform/shorten.pipe";
import {FilterPipe} from "./pipe-transform/filter.pipe";
import {ReversePipe} from "./pipe-transform/reverse.pipe";
import {SortPipe} from "./pipe-transform/sort.pipe";
import {FormsModule} from "@angular/forms";
import { HttpRequestComponent } from './http-request/http-request.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthInterceptorService} from "./http-request/auth-interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
    PipeTransformComponent,
    ShortenPipe,
    FilterPipe,
    ReversePipe,
    SortPipe,
    HttpRequestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
