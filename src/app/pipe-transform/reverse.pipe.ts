import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {
  transform(value: string): unknown {
    if(!value || value.length <= 0)
      return value;

    return [...value].reverse().join("");
  }

}
