import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType, HttpHeaders, HttpParams} from "@angular/common/http";
import {Post} from "./post.model";
import {catchError, map, Subject, tap, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  error = new Subject<string>();
  constructor(private httpClient: HttpClient) { }

  createAndStorePost(title:string, content: string) {
    const postData: Post = {
      title,
      content
    }
    return this.httpClient.post<{name: string}>(
      'https://ng-complete-guide-15b7c-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json',
      postData);
  }

  fetchPost() {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('print', 'pretty');
    searchParams =searchParams.append('custom', 'key');
    return this.httpClient.get<{[key:string]: Post}>(
      'https://ng-complete-guide-15b7c-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json', {
        headers: new HttpHeaders({
          "Custom-Header": "Hello",
        }),
        params: searchParams,
        responseType: 'json'
      })
      .pipe(map((response) => {
        const postArray:Post[]  = [];
        for(const key in response) {
          if(response.hasOwnProperty(key)) {
            postArray.push({...response[key], id: key});
          }
        }
        return postArray;
      }), catchError(err => {
        return throwError(err);
      }))
  }

  deletePost() {
    return this.httpClient.delete('https://ng-complete-guide-15b7c-default-rtdb.asia-southeast1.firebasedatabase.app/posts.json', {
      observe: 'events',
      responseType: 'text'
    }).pipe(tap(event => {
      console.log(event);
      if(event.type === HttpEventType.Sent) {
        console.log("Event is sent");
      }
      if(event.type === HttpEventType.Response) {
        console.log(event.body);
      }
    }));
  }
}
