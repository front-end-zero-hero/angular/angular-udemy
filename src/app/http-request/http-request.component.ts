import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs";
import {Post} from "./post.model";
import {PostService} from "./post.service";

@Component({
  selector: 'app-http-request',
  templateUrl: './http-request.component.html',
  styleUrls: ['./http-request.component.scss']
})
export class HttpRequestComponent implements OnInit {
  loadedPosts: Post[] = [];
  isFetching = false;
  error = null;

  constructor(private http: HttpClient, private postService: PostService) {
  }

  ngOnInit() {
    this.onFetchPosts();
  }

  onCreatePost(postData: { title: string; content: string }) {
    this.postService.createAndStorePost(postData.title, postData.content).subscribe(
      response => console.log(response));
  }

  onFetchPosts() {
    this.isFetching = true;
    this.postService.fetchPost()
      .subscribe(posts => {
        this.isFetching = false;
        this.loadedPosts = posts
      }, error => {
        this.isFetching = false;
        this.error = error.message;
        console.log(error);
      });
  }

  onHandleError() {
    this.error = null;
  }

  onClearPosts() {
    this.postService.deletePost().subscribe(response => {
      this.loadedPosts = [];
    });
  }
}
